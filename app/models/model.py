from datetime import datetime


class Contact:
    def __init__(self, name: object = None, cell: object = None, id=None) -> object:
        self._id = id
        self._name = name
        self._cellphone = cell


    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self.setterId(value)

    def setterId(self, value=None):
        if self._id is None:
            self._id = int(datetime.timestamp(datetime.now()) * 1000000)
            return
        self._id = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._cellphone = value

    @property
    def cellphone(self):
        return self._cellphone

    @cellphone.setter
    def cellphone(self, value):
        self._cellphone = value

    def getDict(self):
        return {
            'id': self._id,
            'name': self._name,
            'cellphone': self._cellphone
        }


def getContact(name=None, cell=None):
    return Contact(name, cell)