from app import app
from app.models.model import Contact
from app.db.database import Database
from flask import jsonify, request


c1 = Contact('Nicolas Rodrigues', '5541954122723')
c1.setterId()
c2 = Contact('Davi Lucca Rocha', '5541979210400')
c2.setterId()

db = Database.instance().data

db.append(c1.getDict())
db.append(c2.getDict())


@app.route('/', methods=['GET'])
def root():
    # return jsonify({'message': 'Hello world!'})
    data = [Contact(name=c['name'], cell=c['cellphone'], id=c['id']).getDict() for c in Database.instance().data]
    return jsonify(data)


@app.route('/addcontact', methods=['POST'])
def add():
    _json = request.json
    _name = _json['name']
    _cell = _json['cellphone']
    contact = Contact(name=_name, cell=_cell)
    contact.setterId()
    Database.instance().data.append(contact.getDict())

    return jsonify({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/del/<value>', methods=['DELETE'])
def delete(value):
    Database.instance().data = [c for c in Database.instance().data if c['id'] != int(value)]

    return jsonify({'success': True}), 200, {'ContentType': 'application/json'}

