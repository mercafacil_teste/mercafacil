class Database:
    __instance = None

    def __init__(self) -> None:
        super().__init__()
        self._data = []

    @property
    def data(self):
        return self._data

    @staticmethod
    def instance():
        if not Database.__instance:
            Database.__instance = Database()
        return Database.__instance

    @data.setter
    def data(self, value):
        self._data = value
